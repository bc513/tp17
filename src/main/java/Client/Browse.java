package Client;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.enumerated.BrowseDirection;
import org.eclipse.milo.opcua.stack.core.types.enumerated.BrowseResultMask;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.eclipse.milo.opcua.stack.core.types.structured.BrowseDescription;
import org.eclipse.milo.opcua.stack.core.types.structured.BrowseResult;
import org.eclipse.milo.opcua.stack.core.types.structured.ReferenceDescription;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;

public class Browse {
    public static void main(final String[] args)throws Exception{

        final OpcUaClient client = Client.connect().get();

        try {
            System.out.format("%s %s %s%n", "Name", "Type", "NodeID");
            System.out.println(
                    "==========================================================================================");
           browse(client, Identifiers.RootFolder,"");
        }finally {
            client.disconnect().get();
        }
    }

    private static void browse(OpcUaClient client, NodeId nodeId,String name)throws  Exception{
        final BrowseDescription browseDescription = new BrowseDescription(
                nodeId,
                BrowseDirection.Forward,
                Identifiers.References,
                true,
                uint(NodeClass.Object.getValue()|NodeClass.Variable.getValue()),
                uint(BrowseResultMask.All.getValue())
        );

        BrowseResult browseResult = client.browse(browseDescription).get();
        do {

            for (final ReferenceDescription referenceDescription : browseResult.getReferences()) {
                System.out.format("%s%n",name + referenceDescription.getBrowseName().getName());
                referenceDescription.getNodeClass().toString();
                referenceDescription.getNodeId().local().map(NodeId::toParseableString).orElse("");
                // browse children
                final NodeId childId = referenceDescription.getNodeId().local().orElse(null);
                if (childId != null) {
                    browse(client, childId, name + "  ");
                }
            }

            if (browseResult.getContinuationPoint().isNotNull()) {
                browseResult = client.browseNext(true, browseResult.getContinuationPoint()).get();
            } else {
                browseResult = null;
            }

        } while (browseResult != null);
    }

}
