package Client;

import Server.Server;
import com.google.common.collect.ImmutableList;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfig;
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfigBuilder;
import org.eclipse.milo.opcua.sdk.client.api.identity.IdentityProvider;
import org.eclipse.milo.opcua.sdk.client.api.identity.UsernameProvider;
import org.eclipse.milo.opcua.sdk.client.api.nodes.VariableNode;
import org.eclipse.milo.opcua.stack.client.DiscoveryClient;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.enumerated.ServerState;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;

public class Client {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String UserName = "user";
    private static final String PASSWORD = "password1";
    private ClientData clientData;
    private Server server;
   // private final boolean serverRequired;

    private static OpcUaClientConfig buildConfiguration(final List<EndpointDescription> endpoints) {

        final OpcUaClientConfigBuilder cfg = new OpcUaClientConfigBuilder();

        cfg.setEndpoint(findBest(endpoints));

        return cfg.build();

    }

    public Client(ClientData clientData) throws Exception{
        this.clientData = clientData;
    }

    public static EndpointDescription findBest(final List<EndpointDescription> endpoints) {
        /*
         * We simply assume we have at least one and pick the first one. In a more
         * productive scenario you would actually evaluate things like ciphers and
         * security.
         */
        return endpoints.get(0);
    }

    // create client

    public static CompletableFuture<OpcUaClient> createClient() {
        final String endpoint = String.format("opc.tcp://localhost:3000");

        return DiscoveryClient
                .getEndpoints(endpoint) // look up endpoints from remote
                .thenCompose(endpoints -> {
                    try {
                        return CompletableFuture.completedFuture(OpcUaClient.create(buildConfiguration(endpoints)));
                    } catch (final UaException e) {
                        return null;
                    }
                });
    }

    // connect

    public static CompletableFuture<OpcUaClient> connect() {
        return createClient()
                .thenCompose(OpcUaClient::connect) // trigger connect
                .thenApply(OpcUaClient.class::cast); // cast result of connect from UaClient to OpcUaClient
    }

    // main entry point

    public void run() throws Exception {

        final Semaphore s = new Semaphore(0);

        connect()
                .whenComplete((client, e) -> {

                    if (e == null) {
                        System.out.println("Connected");
                    } else {
                        System.err.println("Failed to connect");
                        e.printStackTrace();
                    }
                })
                .thenCompose(OpcUaClient::disconnect)
                .thenRun(s::release);

        System.out.println("Wait for completion");

        s.acquire();

        System.out.println("login success");
    }


    public static OpcUaClient createClientSync() throws InterruptedException, ExecutionException, UaException {
        final String endpoint = String.format("opc.tcp://localhost:3000");

        final List<EndpointDescription> endpoints = DiscoveryClient.getEndpoints(endpoint)
                .get();

        return OpcUaClient.create(buildConfiguration(endpoints));
    }

    public OpcUaClient connectSync() throws InterruptedException, ExecutionException, UaException {
        final OpcUaClient client = createClientSync();

        client.connect().get();
        VariableNode node = client.getAddressSpace().createVariableNode(Identifiers.Server_ServerStatus_StartTime);
        DataValue value = node.readValue().get();

        logger.info("StartTime = {}",value.getValue().getValue());

        readServerStateAndTime(client).thenAccept(values -> {
            DataValue v0 = values.get(0);
            DataValue v1 = values.get(1);

            logger.info("State={}", ServerState.from((Integer) v0.getValue().getValue()));
            logger.info("CurrentTime={}", v1.getValue().getValue());

           // future.complete(client);
        });

        return client;
    }

    private CompletableFuture<List<DataValue>> readServerStateAndTime(OpcUaClient client) {
        List<NodeId> nodeIds = ImmutableList.of(
                Identifiers.Server_ServerStatus_State,
                Identifiers.Server_ServerStatus_CurrentTime);

        return client.readValues(0.0, TimestampsToReturn.Both, nodeIds);
    }

    public IdentityProvider getIdentityProvider(){
        return new UsernameProvider(UserName,PASSWORD);
    }

}

