package Client;

import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class NodeID {
    private NodeID() {
    }

    private static final Map<NodeId, String> IDS;

    static {
        final LinkedHashMap<NodeId, String> ids = new LinkedHashMap<>();

        for (final Field field : Identifiers.class.getDeclaredFields()) {

            try {
                final Object value = field.get(null);
                if (value instanceof NodeId) {
                    ids.put((NodeId) value, field.getName());
                }
            } catch (final Exception e) {
                continue;
            }
        }

        IDS = Collections.unmodifiableMap(ids);
    }

    public static Optional<String> lookup(final NodeId id) {
        return Optional.ofNullable(IDS.get(id));
    }
}
