package Client;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.AttributeId;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.Arrays.asList;
import static java.util.Collections.nCopies;
import static java.util.Collections.singletonList;
import static org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn.Both;

public class Read{
    private static final NodeId NODE_TO_READ = new NodeId(2, "Vehicles/vehicle/2:Elektromotor/2:Elektromotor_name");

    public static CompletableFuture<DataValue> read(
            final OpcUaClient client,
            final NodeId nodeId) {

        return client.readValue(0, Both, nodeId);
    }

    public static CompletableFuture<List<DataValue>> read(
            final OpcUaClient client,
            final AttributeId attributeId,
            final NodeId... nodeIds) {

        return client
                .read(
                        0,
                        Both,
                        asList(nodeIds),
                        nCopies(nodeIds.length, attributeId.uid()));
    }

    public static void main(final String[] args) throws Exception {

        // == first example

        Client.connect()

                .thenCompose(client -> {
                    return read(client, NODE_TO_READ)
                            .thenAccept(value -> {
                                Values.dumpValues(
                                        System.out,
                                        singletonList(NODE_TO_READ),
                                        singletonList(value));
                            })
                            .thenCompose(v -> client.disconnect());
                })

                .get();

        // == second example

        final OpcUaClient client = Client.connect().get();

        // nodes to read

        final NodeId[] moreIds = new NodeId[] {
                Identifiers.Server_ServerStatus_BuildInfo_ManufacturerName,
                Identifiers.Server_ServerStatus_BuildInfo_ProductName,
                Identifiers.Server_ServerStatus_CurrentTime
        };

        // read values

        final CompletableFuture<List<DataValue>> future = read(
                client,
                AttributeId.Value,
                moreIds);

        final List<DataValue> values = future.get();
        Values.dumpValues(System.out, asList(moreIds), values);

        // read browse name

        final CompletableFuture<List<DataValue>> future2 = read(
                client,
                AttributeId.BrowseName,
                moreIds);

        final List<DataValue> values2 = future2.get();
        Values.dumpValues(System.out, asList(moreIds), values2);

        // disconnect

        DataValue value = client.readValue(0,Both,NODE_TO_READ).get();
        System.out.println((String)value.getValue().getValue());

        client.disconnect().get();
    }

}
