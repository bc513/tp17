package Client;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaSubscription;
import org.eclipse.milo.opcua.stack.core.AttributeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.enumerated.MonitoringMode;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.eclipse.milo.opcua.stack.core.types.structured.MonitoredItemCreateRequest;
import org.eclipse.milo.opcua.stack.core.types.structured.MonitoringParameters;
import org.eclipse.milo.opcua.stack.core.types.structured.ReadValueId;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

import static Client.Client.connect;
import static java.time.Duration.ofMinutes;
import static java.util.Collections.singletonList;
import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;

public class Subscribe {
    public static final NodeId NODE_TO_READ = new NodeId(2, "Vehicles/vehicle/2:Elektromotor/2:Elektromotor_name");
    public static final NodeId NEWNODE = NodeId.parse("ns=2;s=Vehicles/vehicle/2:Elektromotor/2:Elektromotor_id2");

    private static final AtomicInteger clientHandles = new AtomicInteger();

    public static void main(final String[] args) throws Exception {

        final CompletableFuture<OpcUaClient> future = connect().thenCompose(client -> {

            return client.getSubscriptionManager().createSubscription(1_000.0)
                    .thenCompose(subscription -> {

                        return subscribeTo(
                                subscription,
                                AttributeId.Value,
                                NODE_TO_READ,NEWNODE)

                                .thenAccept(result -> {
                                    System.out.println(result);
                                });

                    })
                    .thenApply(v -> client);

        });

        final OpcUaClient client = future.get();

        Thread.sleep(ofMinutes(15).toMillis());

        client.disconnect().get();
    }


    private static CompletableFuture<UaMonitoredItem> subscribeTo(
            final UaSubscription subscription,
            final AttributeId attributeId,
            final NodeId... nodeIds) {

        // subscription request

        final List<MonitoredItemCreateRequest> requests = new ArrayList<>(nodeIds.length);

        for (final NodeId nodeId : nodeIds) {

            final MonitoringParameters parameters = new MonitoringParameters(
                    uint(clientHandles.getAndIncrement()), // must be set
                    1_000.0, // sampling interval
                    null,
                    uint(10),
                    true);

            final ReadValueId readValueId = new ReadValueId(
                    nodeId,
                    attributeId.uid(),
                    null,
                    null);

            final MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(
                    readValueId,
                    MonitoringMode.Reporting,
                    parameters);

            requests.add(request);

        }

        // callback setup

        final BiConsumer<UaMonitoredItem, DataValue> consumer = //
                (item, value) -> Values.dumpValues(
                        System.out,
                        singletonList(item.getReadValueId().getNodeId()),
                        singletonList(value));

        final BiConsumer<UaMonitoredItem, Integer> onItemCreated = //
                (monitoredItem, id) -> monitoredItem.setValueConsumer(consumer);

        // subscribe

        return subscription
                .createMonitoredItems(
                        TimestampsToReturn.Both,
                        requests,
                        onItemCreated // callback setup -> setting the callback
                )
                .thenApply(result -> result.get(0));
    }
}
