package Client;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;

import java.util.concurrent.CompletableFuture;

public class Write {
    private static final NodeId NODE_TO_WRITE = Subscribe.NEWNODE;
    private final static String value = "123";

    public static CompletableFuture<StatusCode> write(
            final OpcUaClient client,
            final NodeId nodeId,
            final Object value) {

        return client.writeValue(nodeId, new DataValue(new Variant(value)));
    }

    public static void main(final String[] args) throws Exception {

        // first example

        Client.connect()

                .thenCompose(client -> {

                    return write(
                            client,
                            NODE_TO_WRITE,
                            value // value to write
                    )

                            .whenComplete((result, error) -> {
                                if (error == null) {
                                    System.out.format("Result: %s%n", result);
                                } else {
                                    error.printStackTrace();
                                }
                            })

                            .thenCompose(v -> client.disconnect());

                }).get();
    }
}
