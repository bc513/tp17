package Client;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.structured.*;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.util.Collections.singletonList;

public class find {

    public static CompletableFuture<BrowsePathResult> translate(
            final OpcUaClient client,
            final NodeId startingNode,
            final String... path) {

        Objects.requireNonNull(startingNode);
        Objects.requireNonNull(path);

        // convert to elements

        final RelativePathElement[] elements = new RelativePathElement[path.length];
        for (int i = 0; i < path.length; i++) {
            elements[i] = new RelativePathElement(
                    Identifiers.HierarchicalReferences,
                    false, true,
                    QualifiedName.parse(path[i]));
        }

        // translate

        final BrowsePath request = new BrowsePath(startingNode, new RelativePath(elements));
        return client.translateBrowsePaths(singletonList(request)).thenApply(response -> response.getResults()[0]);
    }



    public static void main(final String[] args) throws InterruptedException, ExecutionException {
        Client.connect().thenCompose(client -> {
            return translate(client,Identifiers.ObjectsFolder)
                    .thenAccept(find::dumpResult)
                    .thenCompose(c->client.disconnect());
        }).get();
    }

    public static void dumpResult(final BrowsePathResult result) {
        if (result.getStatusCode().isGood()) {

            for (final BrowsePathTarget target : result.getTargets()) {
                System.out.println(target.getTargetId().local().get());
            }

        } else {

            System.out.println(result.getStatusCode());

        }
    }
}
