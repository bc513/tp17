package Client1;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.eclipse.milo.opcua.stack.core.types.structured.AddNodesItem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class AddNode implements ClientData {

    public static void main(String[] args) throws Exception {
        AddNode add = new AddNode();
        new Client(add).run();

    }

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();
        AddNodesItem newItem = new AddNodesItem(
                new NodeId(2,"Vehicles").expanded(),
                Identifiers.Organizes,
                new NodeId(2,"Vehicles/vehicle2").expanded(),
                new QualifiedName(2,"Vehicles/vehicle2"),
                NodeClass.Object,
                null,
                new NodeId(2,"Object/Types/Vehicle").expanded());

        List<AddNodesItem> items = new ArrayList<AddNodesItem>();
        items.add(newItem);

        client.addNodes(items).get();
        future.complete(client);
    }
}
