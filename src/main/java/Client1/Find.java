package Client1;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.nodes.UaObjectNode;
import org.eclipse.milo.opcua.sdk.client.nodes.UaVariableNode;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;
import java.util.concurrent.CompletableFuture;

public class Find implements ClientData {

    public static void main(String[] args) throws Exception {
        Find find = new Find();
        new Client(find).run();
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();

        // start browsing at root folder
        NodeId nodeID = new NodeId(2,"Vehicles/vehicle");

        findNode("", client, nodeID);

        future.complete(client);
    }

    private void findNode(String indent, OpcUaClient client,NodeId nodeId) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter id:");
        int i = scanner.nextInt();
        UaObjectNode uaObjectNode = (UaObjectNode) client.getAddressSpace().createObjectNode(nodeId);
        UaVariableNode uaVariableNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2,"Vehicle_id")).get();
        DataValue value = uaVariableNode.readValue().get();
        if (i == (Integer) value.getValue().getValue()) {
                logger.info("find success");
                System.out.println((Integer) value.getValue().getValue());
                UaVariableNode uaVariableNode1 = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2,"Vehicle_Credit")).get();
                DataValue value1 = uaVariableNode1.readValue().get();
                logger.info("Kosten: " + value1.getValue().getValue());
            } else {
                System.out.println("can not find");
            }

    }


}
