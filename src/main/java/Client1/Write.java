package Client1;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

public class Write implements ClientData {

    public static void main(String[] args) throws Exception {
        Write write = new Write();

        new Client(write).run();
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();

        NodeId nodeId = new NodeId(2, "Vehicles/vehicle/2:Elektromotor/2:Elektromotor_credit");

        Variant v = new Variant(20);

        DataValue dv = new DataValue(v, null, null);
        nodeId.getIdentifier();

        CompletableFuture<StatusCode> f =
                client.writeValue(nodeId, dv);

        StatusCode statusCodes = f.get();
        if (statusCodes.isGood()) {
            logger.info("Wrote '{}' to nodeId={} statusCode = {}", v, nodeId, statusCodes);
        }

        future.complete(client);
    }

}
