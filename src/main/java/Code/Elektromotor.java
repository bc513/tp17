package Code;

public class Elektromotor {
    
    private int credit;		//## attribute credit

    private String name;		//## attribute name

    private Vehicle vehicle;		//## link itsVehicle
    

    public  Elektromotor() {
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}


