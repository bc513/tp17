package Code;

public class Fahrwerkkomponente {

    private int credit;        //## attribute credit

    private String name;        //## attribute name

    private Vehicle itsVehicle;        //## link itsVehicle




    public Fahrwerkkomponente() {
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vehicle getItsVehicle() {
        return itsVehicle;
    }

    public void setItsVehicle(Vehicle itsVehicle) {
        this.itsVehicle = itsVehicle;
    }
}