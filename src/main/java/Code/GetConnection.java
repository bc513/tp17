package Code;

import java.sql.*;

public class GetConnection {
    public static Connection connection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("drive success");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = "jdbc:mysql://localhost:3306/carrecycle?serverTimezone=GMT";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url,"root","simtrade");
            System.out.println("connected");
        } catch (SQLException e) {
            System.out.println("no connected");
            e.printStackTrace();
        }
        return conn;
    }
}
