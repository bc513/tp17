package Code;

public class HochVoltBatterie {

    private float gewicht;		//## attribute gewicht

    private String produktionsJahr;		//## attribute produktionsJahr

    private String stateOfHealth;		//## attribute stateOfHealth

    //private Energiespeicher energiespeicher;		//## link itsEnergiespeicher

    private int credit;
    

    

    public  HochVoltBatterie() {}

    public float getGewicht() {
        return gewicht;
    }

    public void setGewicht(float gewicht) {
        this.gewicht = gewicht;
    }

    public String getProduktionsJahr() {
        return produktionsJahr;
    }

    public void setProduktionsJahr(String produktionsJahr) {
        this.produktionsJahr = produktionsJahr;
    }

    public String getStateOfHealth() {
        return stateOfHealth;
    }

    public void setStateOfHealth(String stateOfHealth) {
        this.stateOfHealth = stateOfHealth;
    }


//    public Energiespeicher getEnergiespeicher() {
//        return energiespeicher;
//    }
//
//    public void setEnergiespeicher(Energiespeicher energiespeicher) {
//        this.energiespeicher = energiespeicher;
//    }
}

