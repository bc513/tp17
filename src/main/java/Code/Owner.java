package Code;

import java.text.ParseException;
import java.util.Date;

public class Owner extends User {

    public Owner(String email, int id, String geburtstag, String name, String username, String password) throws ParseException {
        super(email,id,geburtstag,name,username,password,"Ownner");
    }

    public void relaseVehicleint(int fahrzeugkomponente_id,int elektromotor_id,int energiespeicher_id,String marke){
        VehicleManagement.getInstance().addVehicle(this.getId(),fahrzeugkomponente_id,elektromotor_id,energiespeicher_id,marke);
    }
    
}
