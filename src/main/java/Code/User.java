package Code;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User {

    private String email;        //## attribute Email

    private int id;        //## attribute ID

    private float credit = 100;        //## attribute credit

    private Date geburtstag;        //## attribute geburtstag

    private String name;        //## attribute name

    private String username;        //## attribute username

    private  String usertype;

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    protected String password;

    public User(){}

    public User(String email, int id, String geburtstag, String name, String username, String password, String usertype) throws ParseException {
        this.email = email;
        this.id = id;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        this.geburtstag = fmt.parse(geburtstag);
        this.name = name;
        this.username = username;
        this.password = password;
        this.usertype=usertype;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }


    public float getCredit() {
        return credit;
    }


    public String getGeburtstag() {
        return geburtstag.toString();
    }

    public void setGeburtstag(String geburtstag) throws ParseException {
        SimpleDateFormat dfm = new SimpleDateFormat("yyy-MM-dd");
        this.geburtstag = dfm.parse(geburtstag);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void release() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    @Override
    public String toString() {
        return "email:"+getEmail()+"\n"
                +"geburtstag:"+getGeburtstag()+"\n"+
                "name:"+getName()+"\n"+
                "userName:"+getUsername()+"\n"+
                "credit:"+getCredit()+"\n"+
                "id:"+getId();


    }
}
