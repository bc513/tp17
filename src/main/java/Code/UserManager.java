package Code;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;


public class UserManager {
    private UserManager() {
        getDBUser();
    };

    public static UserManager um = new UserManager();

    public static UserManager getInstance(){
        return um;
    }

    private List<User> list = new ArrayList<User>();

    public List<User> getUser(){
        return list;
    }

    // get the existing user-daten
    private void getDBUser() {
        Connection conn = GetConnection.connection();
        String sql = "select * from user";
        try {
            PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            int id;
            String name;
            String username;
            String email;
            String password;
            Date geburtstag;
            String usertype;
            float credit;
            while(rs.next()){
                id = rs.getInt(1);
                name = rs.getString(2);
                username = rs.getString(3);
                password = rs.getString(4);
                geburtstag = new Date(rs.getDate(5).getTime());
                credit = rs.getFloat(6);
                email = rs.getString(7);
                usertype=rs.getString((8));
                User u = new User(email,id,geburtstag.toString(),name,username,password,usertype);
                if(usertype.equals("Owner")) {
                    User o = new Owner(email,id,geburtstag.toString(),name,username,password);
                } else {
                    User re = new Recycler(email,id,geburtstag.toString(),name,username,password);
                }
                list.add(u);
            }
            rs.close();
            pst.close();
            conn.close();
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }


    }


    public boolean addOwner(String username, String name, String email, String geburtstag,String password) throws ParseException {
        boolean userExist = addUser(username,name,email,geburtstag,password);
        if (userExist == false) return false;

        return true;
    }

    public boolean addRecycler(String username, String name, String email, String geburtstag,String password) throws ParseException, SQLException {
        boolean userExists = addUser(username,name,email,geburtstag,password);
        if (userExists == false) return false;

        return true;
    }


    // add to user table
    private boolean addUser(String username, String name, String email, String geburtstag,String password) throws ParseException {
        for (User user:list) {
            if (user.getUsername().equals(username)){
                return false;
            }
        }
        Date birthday = getBirth(geburtstag);
        long birthdayL = birthday.getTime();

        // sql operation
        Connection conn = GetConnection.connection();
        String sql ="insert into user (name,user_name,password,birthday,credit,email) values(?,?,?,?,?,?)";
        PreparedStatement pstmt;
        try{
            pstmt = (PreparedStatement)conn.prepareStatement(sql);
            pstmt.setString(1,name);
            pstmt.setString(2,username);
            pstmt.setString(3,password);
            pstmt.setDate(4,new java.sql.Date(birthdayL));
            pstmt.setFloat(5,100);
            pstmt.setString(6,email);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
            System.out.println("insert success");
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        getDBUser();
        return true;
    }





    private Date getBirth(String ge) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date birthdy = sdf.parse(ge);
        return birthdy;
    }


    @Override
    public String toString() {
        String tos = "";
        for (User user:list
             ) {tos = tos+user.toString()+"\n";
        }
        return tos;
    }
}
