package Code;

public class Vehicle {

    private int vehicle_id;

    private int fahrzeugkomponente_id;

    private int elektromotor_id;

    private int energiespeicher_id;

    private String marke;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    private int user_id;


    public Vehicle(int vehicle_id, int fahrzeugkomponente_id, int elektromotor_id, int energiespeicher_id, String marke,int user_id) {
        this.vehicle_id = vehicle_id;
        this.fahrzeugkomponente_id = fahrzeugkomponente_id;
        this.elektromotor_id = elektromotor_id;
        this.energiespeicher_id = energiespeicher_id;
        this.marke = marke;
        this.user_id=user_id;
    }

    public int getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(int vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public int getFahrzeugkomponente_id() {
        return fahrzeugkomponente_id;
    }

    public void setFahrzeugkomponente_id(int fahrzeugkomponente_id) {
        this.fahrzeugkomponente_id = fahrzeugkomponente_id;
    }

    public int getElektromotor_id() {
        return elektromotor_id;
    }

    public void setElektromotor_id(int elektromotor_id) {
        this.elektromotor_id = elektromotor_id;
    }

    public int getEnergiespeicher_id() {
        return energiespeicher_id;
    }

    public void setEnergiespeicher_id(int energiespeicher_id) {
        this.energiespeicher_id = energiespeicher_id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }
}

