package Server;

import Code.User;

import Code.UserManager;
import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfig;
import org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfigBuilder;
import org.eclipse.milo.opcua.sdk.server.identity.AnonymousIdentityValidator;
import org.eclipse.milo.opcua.sdk.server.identity.CompositeValidator;
import org.eclipse.milo.opcua.sdk.server.identity.UsernameIdentityValidator;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.security.CertificateValidator;
import org.eclipse.milo.opcua.stack.core.security.DefaultCertificateManager;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.server.EndpointConfiguration;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;


public class Server {
    public static void main(final String[] args) throws Exception {
        File securityTempDir = new File(System.getProperty("java.io.tmpdir"), "security");
        if (!securityTempDir.exists() && !securityTempDir.mkdirs()) {
            try {
                throw new Exception("unable to create security temp dir: " + securityTempDir);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        LoggerFactory.getLogger(Server.class).info("security temp dir: {}", securityTempDir.getAbsolutePath());

        final OpcUaServerConfigBuilder builder = new OpcUaServerConfigBuilder();
        builder.setIdentityValidator(new CompositeValidator(AnonymousIdentityValidator.INSTANCE));

        final org.eclipse.milo.opcua.stack.server.EndpointConfiguration.Builder endpointBuilder = new EndpointConfiguration.Builder();

        endpointBuilder.addTokenPolicies(OpcUaServerConfig.USER_TOKEN_POLICY_ANONYMOUS);

        endpointBuilder.setSecurityPolicy((SecurityPolicy.None));

        endpointBuilder.setBindPort(3000);
        builder.setEndpoints(Collections.singleton(endpointBuilder.build()));

        builder.setApplicationName(LocalizedText.english("Fahrzeuge Marketing"));
        String uri = "opc.tcp:localhost";
        builder.setApplicationUri(uri);
        builder.setCertificateManager(new DefaultCertificateManager());

        builder.setCertificateValidator(new CertificateValidator() {
            @Override
            public void validate(X509Certificate certificate) throws UaException {

            }

            @Override
            public void verifyTrustChain(List<X509Certificate> certificateChain) throws UaException {

            }
        });

        UsernameIdentityValidator identityValidator = new UsernameIdentityValidator(
           true,
               authenticationChallenge -> {
               String username = authenticationChallenge.getUsername();
               String password = authenticationChallenge.getPassword();
                   List<User> userList = UserManager.getInstance().getUser();
                   for (User user:userList) {
                       if(user.getUsername().equals(username)){
                           if(user.getPassword().equals(password)){
                               return true;
                           }
                           else {
                               return false;
                           }
                       }
                   }
                   return false;
               }
        );

        OpcUaServer server = new OpcUaServer(builder.build());
        // server.getAddressSpaceManager().register(new Client.Client.Client.Client.Server.CustomNamespace(server,Client.Client.Client.Client.Server.CustomNamespace.URI));
        //server.getAddressSpaceManager().register(new CustomNamespace(server, CustomNamespace.URI));

        server.getAddressSpaceManager().register(new ObjectTypeNameSpace(server,ObjectTypeNameSpace.URI));

        server.startup().get();

        Thread.sleep(Long.MAX_VALUE);
    }

}
